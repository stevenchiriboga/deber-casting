package com.company;

public class Main {

    public static void main(String[] args) {
	Object o = new Object();
	Fruit f = new Fruit();
	Apple a = new Apple();
	Citrus c = new Citrus();
	Orange or = new Orange();
	Squeezazble s = new Squeezazble() {};

	    //CAST objeto
	    try {
           o=(Fruit)f;
           System.out.println("Se puede cast objeto a fruta");
        }
         catch (Exception e) {
           System.err.println("No se puede hacer casting objeto a fruta");
        }
        try {
            o=(Apple)a;
            System.out.println("Se puede cast objeto a manzana");
        }
        catch (Exception e) {
            System.err.println("No se puede hacer casting objeto a manzana");
        }

        try {
            o=(Citrus)c;
            System.out.println("Se puede cast objeto a citrico");
        }
        catch (Exception e) {
            System.err.println("No se puede hacer casting objeto a citrico");
        }
        try {
            o=(Orange)or;
            System.out.println("Se puede cast objeto a naranja");
        }
        catch (Exception e) {
            System.err.println("No se puede hacer casting objeto a naranja");
        }
        try {
            o=(Squeezazble)s;
            System.out.println("Se puede cast objeto a squeezable");
        }
        catch (Exception e) {
            System.err.println("No se puede hacer casting objeto a squezable");
        }

        //CAST fruta

        try {
            f=(Fruit)f;
            System.out.println("Se puede cast fruta a fruta");
        }
        catch (Exception e) {
            System.err.println("No se puede hacer casting fruta a fruta");
        }
        try {
            f=(Apple)a;
            System.out.println("Se puede cast fruta a manzana");
        }
        catch (Exception e) {
            System.err.println("No se puede hacer casting fruta a manzana");
        }

        try {
            f=(Citrus)c;
            System.out.println("Se puede cast fruta a citrico");
        }
        catch (Exception e) {
            System.err.println("No se puede hacer casting fruta a citrico");
        }
        try {
            f=(Orange)or;
            System.out.println("Se puede cast fruta a naranja");
        }
        catch (Exception e) {
            System.err.println("No se puede hacer casting fruta a naranja");
        }

        //CAST manzana

        try {
            a = (Apple) f;
            System.out.println("Se puede cast manzana a fruta");
        }
        catch (Exception e) {
            System.err.println("No se puede hacer casting manzana a fruta");
        }
        try {
            a=(Apple)a;
            System.out.println("Se puede cast manzana a manzana");
        }
        catch (Exception e) {
            System.err.println("No se puede hacer casting manzana a manzana");
        }
        try {
            a= (Apple) s;
            System.out.println("Se puede cast manzana a squeezable");
        }
        catch (Exception e) {
            System.err.println("No se puede hacer casting manzana a squezable");
        }

        //CAST Citrus

        try {
            c= (Citrus) f;
            System.out.println("Se puede cast citrico a fruta");
        }
        catch (Exception e) {
            System.err.println("No se puede hacer casting citrico a fruta");
        }
        try {
            c=(Citrus)c;
            System.out.println("Se puede cast citrico a citrico");
        }
        catch (Exception e) {
            System.err.println("No se puede hacer casting citrico a citrico");
        }
        try {
            c=(Orange)or;
            System.out.println("Se puede cast citrico a naranja");
        }
        catch (Exception e) {
            System.err.println("No se puede hacer casting citrico a naranja");
        }
        try {
            c= (Citrus) s;
            System.out.println("Se puede cast citrico a squeezable");
        }
        catch (Exception e) {
            System.err.println("No se puede hacer casting citrico a squezable");
        }

        //CAST orange

        try {
            or= (Orange) f;
            System.out.println("Se puede cast naranja a fruta");
        }
        catch (Exception e) {
            System.err.println("No se puede hacer naranja objeto a fruta");
        }
        try {
            or= (Orange) c;
            System.out.println("Se puede cast naranja a citrico");
        }
        catch (Exception e) {
            System.err.println("No se puede hacer casting naranja a citrico");
        }
        try {
            or=(Orange)or;
            System.out.println("Se puede cast naranja a naranja");
        }
        catch (Exception e) {
            System.err.println("No se puede hacer casting naranja a naranja");
        }
        try {
            or= (Orange) s;
            System.out.println("Se puede cast naranja a squeezable");
        }
        catch (Exception e) {
            System.err.println("No se puede hacer casting naranja a squezable");
        }
    }
}
